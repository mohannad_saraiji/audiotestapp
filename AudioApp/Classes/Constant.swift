//
//  Constant.swift
//  AudioApp
//
//  Created by Mohannad Saraiji on 22/09/2018.
//  Copyright © 2018 Mohannad. All rights reserved.
//

import Foundation

class Constant {
    
    struct  URls {
        static let webSocketEndPoint: URL = URL(string:"ws://demos.kaazing.com/echo")!
        static let audioURL: URL = URL(string:"http://stream.radioreklama.bg/radio1.aac")!
        static let youTubeVideURL: URL = URL(string:"https://www.youtube.com/watch?v=QPDX91iJ_RA")!
    }
    
    static let  pop = "pop"
}
