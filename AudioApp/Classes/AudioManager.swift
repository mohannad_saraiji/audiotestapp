//
//  AudioManager.swift
//  AudioApp
//
//  Created by Mohannad Saraiji on 22/09/2018.
//  Copyright © 2018 Mohannad. All rights reserved.
//

import UIKit
import AVFoundation

class AudioManager {
    
    static let shared = AudioManager()
    var player: AVPlayer!
    
    func Play(fromUrl: URL)  {
        let playerItem: AVPlayerItem = AVPlayerItem(url: fromUrl)
        player = AVPlayer(playerItem: playerItem)
        player.volume = 1.0
        player.play()
    }
    
    func stop() {
        player.pause()
    }
}
