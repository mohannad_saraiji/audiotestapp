//
//  ViewController.swift
//  AudioApp
//
//  Created by Mohannad Saraiji on 22/09/2018.
//  Copyright © 2018 Mohannad. All rights reserved.
//
import UIKit
import Rswift

class HomeViewController: UIViewController {

    @IBAction func watchButtonTapped(_ sender: UIButton) {
        let liveViewController = R.storyboard.main.liveViewController()
        self.present(liveViewController!, animated: true, completion: nil)
    }
}

