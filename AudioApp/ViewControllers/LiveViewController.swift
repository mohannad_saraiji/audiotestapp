//
//  SecondViewController.swift
//  AudioApp
//
//  Created by Mohannad Saraiji on 22/09/2018.
//  Copyright © 2018 Mohannad. All rights reserved.
//

import UIKit
import Starscream
import YouTubePlayer

class LiveViewController: UIViewController {
    
    // Outlets
    @IBOutlet weak var youtubePlayerView: YouTubePlayerView!
    @IBOutlet weak var closeButton: UIButton!
    
    // Init the Sockect here
    let socket = WebSocket(url: Constant.URls.webSocketEndPoint)

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        closeConnections()
    }
}

//MARK: setup & Funcations

extension LiveViewController {
    
    func setup() {
        
        // init Sockect
        socket.delegate = self
        socket.connect()
        
        // play Sound
        AudioManager.shared.Play(fromUrl: Constant.URls.audioURL)
        
        // play youtubeVido
        youtubePlayerView.delegate = self
        youtubePlayerView.loadVideoURL(Constant.URls.youTubeVideURL)
        youtubePlayerView.isUserInteractionEnabled = false
    }
    
    func closeConnections() {
        AudioManager.shared.stop()
        socket.disconnect()
    }
    
    @objc func sendMessageToSocket() {
        socket.write(string: Constant.pop )
    }
    
    @objc func HideCloseButton() {
        closeButton.isHidden = true
    }
}

//MARK: WebSocketDelegate
extension LiveViewController: WebSocketDelegate {
 
    func websocketDidConnect(socket: WebSocketClient) {
        
        _ = Timer.scheduledTimer(timeInterval: 7.0, target: self, selector: #selector(sendMessageToSocket), userInfo: [], repeats: true)
        
        socket.write(string: "Hello socket :)")
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        print("websocket is Disconnect")
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        print("Recived : \(text)")
        
        if (text == Constant.pop) {
            self.closeButton.isHidden = false
            Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(HideCloseButton), userInfo: [], repeats: false)
        }
    }
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        print("websocket is DidReceiveData")
    }
}

//MARK: YouTubePlayerDelegate
extension LiveViewController: YouTubePlayerDelegate {

    func playerReady(_ videoPlayer: YouTubePlayerView)
    {
          self.youtubePlayerView.mute()
          self.youtubePlayerView.play()
    }
}
